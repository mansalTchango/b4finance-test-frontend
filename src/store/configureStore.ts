import { applyMiddleware, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import appReducer from "./reducers";

const persistConfig = {
  key: 'root',
  storage: storage,
  whitelist: ['root'] // which reducer want to store
};

const pReducer = persistReducer(persistConfig, appReducer);

const middleware = applyMiddleware(thunk, logger);

const store = createStore(pReducer, middleware);


const persistor = persistStore(store);

export { persistor, store };


