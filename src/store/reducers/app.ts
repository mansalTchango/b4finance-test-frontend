import { Action, ActionType, IState, WinType, MsgType } from '../actionsTypes/app.type';

const initialState = {
	ui: {
		loading: false,
	},
	cartes: [],
	start: false,
	time: 0,
	myTime: 0,
	win: WinType.WELCOME,
	msg: MsgType.WELCOME
};

const reducer = (state: IState = initialState, action: Action) => {
	switch (action.type) {
		
		case ActionType.START_LOADING: {
			return {
				...state,
				ui: {
					...state.ui,
					loading: true,
				},
			};
		}

		case ActionType.STOP_LOADING: {
			return {
				...state,
				ui: {
					...state.ui,
					loading: false,
				},
			};
		}

		case ActionType.START_GAME: {
			return {
				...state,
				cartes: {
					...action.data,
				},
				start: true,
				win: WinType.START,
				msg: MsgType.START
			};
		}

		case ActionType.STOP_GAME: {
			return {
				...state,
				start: false,
				win: WinType.LOST,
				msg: MsgType.LOST
			};
		}

		case ActionType.WIN_GAME: {
			return {
				...state,
				start: false,
				win: WinType.WIN,
				msg: MsgType.WIN

			};
		}

		case ActionType.BEST_GAME: {
			return {
				...state,
				time: action.time,
				myTime: action.myTime
			};
		}

		default: {
			return state;
		}
	}
};

export default reducer;
