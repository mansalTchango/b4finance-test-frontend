import { Dispatch } from 'redux';
import { apiProvider } from '../../services/provider';
import { Action, ActionType } from '../actionsTypes/app.type';
import { ICarte } from '../../typings/carte';
import { IGame } from '../../typings/game';


export const startGame = () => async (dispatch: Dispatch<Action>)=> {
    try{
        dispatch({type: ActionType.START_LOADING});

        const resAllCarte: ICarte[] = await apiProvider.getAll(`cartes/getAll`)

        if(resAllCarte.length !== 0){
            dispatch({
                type: ActionType.START_GAME, 
                data: resAllCarte
            });
        }else{
            alert("erreur serveur")
        }


        dispatch({type: ActionType.STOP_LOADING});

		return resAllCarte

    }catch(e){
        dispatch({type: ActionType.STOP_LOADING});
        console.error(e);
        return [];
    }
}

export const stopGame = () => async (dispatch: Dispatch<Action>) => {
    dispatch({type: ActionType.STOP_GAME});
}

export const winGame = () => async (dispatch: Dispatch<Action>) => {
    dispatch({type: ActionType.WIN_GAME});
}

export const getbestGame = () => async (dispatch: Dispatch<Action>) => {
    try{
        dispatch({type: ActionType.START_LOADING});

        const game: IGame = await apiProvider.getAll(`games/getBestGame`)

        dispatch({
            type: ActionType.BEST_GAME, 
            time: game.score || 0,
            myTime: 0
        });

        dispatch({type: ActionType.STOP_LOADING});

        return;

    }catch(e){
        dispatch({type: ActionType.STOP_LOADING});
        console.error(e);
    }
}

export const postWinGame = (time: number) => async (dispatch: Dispatch<Action>) => {
    try{

        dispatch({type: ActionType.START_LOADING});
        const payload = {
            score: time
        }

        await apiProvider.post(`games/create`, payload)
        const game: IGame = await apiProvider.getAll(`games/getBestGame`)
        dispatch({
            type: ActionType.BEST_GAME, 
            time: game.score,
            myTime: time
        });

        dispatch({type: ActionType.STOP_LOADING});

        return;

    }catch(e){
        dispatch({type: ActionType.STOP_LOADING});
        console.error(e);
    }
}