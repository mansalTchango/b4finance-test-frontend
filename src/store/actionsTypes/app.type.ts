import { ICarte } from "../../typings/carte";

export enum ActionType {
    START_LOADING = 'START_LOADING',
    STOP_LOADING = 'STOP_LOADING',
    START_GAME = 'START_GAME',
    STOP_GAME = 'STOP_GAME',
    WIN_GAME = 'WIN_GAME',
    BEST_GAME = 'BEST_GAME'
}

export enum WinType {
    WELCOME = 'info',
    START = 'warning',
    LOST = 'error',
    WIN = 'success'
}

export enum MsgType {
    WELCOME = 'Bienvenu, appuyez sur start pour commencer à jouer',
    START = 'Bonne chance !',
    LOST = 'Dommage, tu feras mieux la prochaine fois !',
    WIN = 'Bien joué, tu as gagné !'
}

interface actionsStartLoading {
    type: ActionType.START_LOADING;
}

interface actionsStopLoading {
    type: ActionType.STOP_LOADING;
}

interface actionsStartGame {
    type: ActionType.START_GAME;
    data: object; //a définir user interface
}

interface actionsStopGame {
    type: ActionType.STOP_GAME;
}

interface actionsWinGame {
    type: ActionType.WIN_GAME;
}

interface actionsBestGame {
    type: ActionType.BEST_GAME;
    time: number; //a définir user interface
    myTime: number;
}

export type Action = actionsStartLoading | actionsStopLoading | actionsStartGame | actionsStopGame | actionsWinGame | actionsBestGame ;

export interface IState {
    ui: {
		loading: boolean;
	},
	cartes: ICarte[], // a definir user interface
    start: boolean,
    time: number,
    myTime: number,
    win: WinType,
    msg: MsgType

}