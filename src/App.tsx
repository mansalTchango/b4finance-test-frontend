import React from 'react';
import Game from './pages/game';
import './assets/scss/App.scss'


function App() {
  return (
      <Game />
  );
}

export default App;
