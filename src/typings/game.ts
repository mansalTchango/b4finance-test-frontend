export interface IGame {
    id: number;
    score: number; 
    createdAt?: Date;
    updatedAt?: Date; 
}