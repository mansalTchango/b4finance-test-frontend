export interface ICarte {
    id: number;
    name: string;
    image: string;
    selected?: boolean;   
    createdAt?: Date;
    updatedAt?: Date; 
}