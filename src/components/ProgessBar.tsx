import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';
import { styled } from '@mui/material/styles';
import { stopGame, postWinGame } from "../store/actions/app";
import { useTypedSelector } from '../hooks/useTypeSelector';
import {IState} from "../store/actionsTypes/app.type";
import { useDispatch } from 'react-redux';


const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
    },
  }));

const ProgressBar = () => {
    const dispatch = useDispatch();
    const { start, win }:IState = useTypedSelector((state) => state.App);

    const [progress, setProgress] = useState(0);
    const [time, setTime] = useState(0);
    const [isActive, setIsActive] = useState(start);

    useEffect(() => {
        if(win === 'success'){
            dispatch(postWinGame(time))
        }
    }, [win])

    useEffect(() => {
        setIsActive(start)

        if(start === true){            
            setProgress(0)
            setTime(0)
        }

    }, [start])

    useEffect(() => {
        var interval: number  = 0;

        if (isActive) {
            interval = window.setTimeout(() => {
                setTime(time + 1);
                setProgress((prevProgress) => (prevProgress >= 100 ? 0 : prevProgress + 1.65));
                
        }, 1000);
        } else if (!isActive && progress !== 0) {
            clearInterval(interval);
        }
        return () => {
            clearInterval(interval);
            if(progress >= 100){
                dispatch(stopGame())
            }
        }
    }, [isActive, progress]);

    return (
        <Box sx={{ width: '100%' }}>
            <Box sx={{ flexGrow: 1 }}>
                <BorderLinearProgress variant="determinate" value={progress} />
            </Box>
        </Box>
    );
}

export default ProgressBar;
