import React, {useState, useEffect} from 'react';
import Stack from '@mui/material/Stack';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert, { AlertProps } from '@mui/material/Alert';
import { useTypedSelector } from '../hooks/useTypeSelector';
import {IState} from "../store/actionsTypes/app.type";

const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref,
  ) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

const SnackbarGame = () => {
  const { win, msg, myTime }:IState = useTypedSelector((state) => state.App);
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpen(false);
  };

  useEffect(() => {
      handleClick()
  }, [win])

  return (
    <Stack spacing={2} sx={{ width: '100%' }}>
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity={win}>
          {win === 'success' ? `${msg}. Votre score est de ${myTime} secondes` : msg}
        </Alert>
      </Snackbar>
    </Stack>
  );
}


export default SnackbarGame;
  