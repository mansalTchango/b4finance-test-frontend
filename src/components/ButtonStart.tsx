import React, {useEffect, useState} from "react";
import Button from '@mui/material/Button';
import Chip from '@mui/material/Chip';
import Stack from '@mui/material/Stack';
import FaceIcon from '@mui/icons-material/Face';
import { useTypedSelector } from '../hooks/useTypeSelector';
import {IState} from "../store/actionsTypes/app.type";

type CarteProps = {
  function: Function
}

const ButtonStart = (props: CarteProps) => {
  const { time, win }:IState = useTypedSelector((state) => state.App);
  const [title, setTitle] = useState(`Meilleur score : ${time} seconds`)

  useEffect(() => {
    setTitle(`Meilleur temps : ${time} seconds`)
  }, [time])

  return(
      <Stack direction="row" spacing={18}>
        <Button variant="contained" onClick={() => props.function()} >Start</Button>
        <Chip icon={<FaceIcon />} label={title} variant="outlined" />
    </Stack>      
  )
}

export default ButtonStart;