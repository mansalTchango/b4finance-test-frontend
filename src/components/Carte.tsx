import React from "react";
import { ICarte } from "../typings/carte";
import { useTypedSelector } from '../hooks/useTypeSelector';
import {IState} from "../store/actionsTypes/app.type";

type CarteProps = {
  carte: ICarte, 
  index:number, 
  isFlipped: boolean,  
  countSelected: number,
  flipCard: Function,
}

const Carte = (props: CarteProps) => {
    const { start }:IState = useTypedSelector((state) => state.App);
    const {carte, index, isFlipped, countSelected, flipCard} = props
    
    return(
        <div
          className={`game-card ${isFlipped ? "flipped" : ""} ${(countSelected === 2 || isFlipped || !start) ? 'is-disabled' : ''}`}
          key={index}
          onClick={() => flipCard(index)}
        >
        <div className="inner">
          <div className="front">
            <img
              src={carte.image}
              alt={carte.name}
              width="90%"
            />
          </div>
          <div className="back"></div>
        </div>
      </div>
    )
}

export default Carte;