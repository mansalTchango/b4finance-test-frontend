import React, { useEffect, useState } from "react";
import Carte from "../../components/Carte";
import { ICarte } from "../../typings/carte";
import ButtonStart from "../../components/ButtonStart";
import { startGame, stopGame, winGame, getbestGame } from "../../store/actions/app";
import ProgressBar from "../../components/ProgessBar";
import SnackbarGame from "../../components/Snackbar";
import { useTypedSelector } from '../../hooks/useTypeSelector';
import {IState} from "../../store/actionsTypes/app.type";
import { useDispatch } from 'react-redux';

const Game = () => {
  const dispatch = useDispatch();
  const { cartes, win, start }:IState = useTypedSelector((state) => state.App);

  const [openedCart, setOpenedCart] = useState<number[]>([]);
  const [matched, setMatched] = useState<number[]>([]);
  const [stateCartes, setStateCartes] = useState<ICarte[]>(cartes);

  useEffect(() => {
    dispatch(getbestGame())
  }, [])

  useEffect(() => {
    if(matched.length !== 0 && matched.length === stateCartes.length / 2){
      dispatch(winGame())

    }
  }, [matched.length])

  useEffect(() => {
    if(start === true){
      setOpenedCart([])
      setMatched([])
    }
 
  }, [start])

  useEffect(() => {
    if (openedCart.length < 2) return;

    const firstMatched = stateCartes[openedCart[0]];
    const secondMatched = stateCartes[openedCart[1]];

    if (secondMatched && firstMatched.id === secondMatched.id) {
      setMatched([...matched, firstMatched.id]);
    }

    if (openedCart.length === 2) setTimeout(() => setOpenedCart([]), 1000);
  }, [openedCart]);

  function flipCard(index: number) {
    setOpenedCart((opened: number[]) => [...opened, index]);
  }

  const getAllCarte = async () => {

    if(win === 'warning'){
      dispatch(stopGame())
    }
    const resStartGame = await dispatch(startGame()) as unknown as ICarte[]
    await setStateCartes(resStartGame);
  }
  
  return (
    <div className="App">
      <SnackbarGame />
      <div className="block">
        <ButtonStart function={async () => await getAllCarte()}/>
      </div>
      <div className="block">
        <div className="cards">
          {
          stateCartes.length !== 0 ?
          stateCartes.map((game, index) => {
            //lets flip the card

            let isFlipped = false;
            if (openedCart.includes(index)) isFlipped = true;
            if (matched.includes(game.id)) isFlipped = true;

            const carteProps = {
              carte: game,
              index: index,
              isFlipped: isFlipped,
              countSelected : openedCart.length,
              flipCard: flipCard
            }

            return (
              <Carte key={index} {...carteProps} />
            );
          })
        :
          <div className={`game-card `}>          
          </div>
        }
        </div>
      </div>
      <div className="block">
        <ProgressBar />
      </div>

    </div>
  );
}

export default Game;

