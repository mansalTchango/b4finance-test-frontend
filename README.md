B4finance-test Frontend
===============

Projet de `test` Mansal Tchango pour B4finance 

Installation du frontend
===============

`Download source code` 

1 - Run commande install node
---------------

`npm insatll` ou `yarn insatll` 

2 - Start le serveur
---------------
`npm start`

4 - Tests fonctionels Cypress
---------------

Afin d'utiliser Cypress vous devez l'installer puis lancer la commande suivante : `sudo yarn run cypress open`

5 - Battre le record
---------------

Essayez de battre le record de 33 secondes :smiley:

<img src="src/assets/image/capture.png" width="400">

